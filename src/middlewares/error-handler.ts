import { NextFunction, Request, Response } from 'express'
import { CustomError } from '../errors'

const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof CustomError) {
    return res.status(err.statusCode).json({
      errors: err.serializeErrors()
    })
  }

  res.status(500).send({ errors: [{ message: 'Something went wrong' }] })
}

export { errorHandler }
